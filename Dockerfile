FROM nginx:latest

COPY ./app/index.html /usr/share/nginx/html/index.html
COPY ./app/audio /usr/share/nginx/html/audio
COPY ./app/css /usr/share/nginx/html/css
COPY ./app/images /usr/share/nginx/html/images
COPY ./app/js /usr/share/nginx/html/js
COPY ./app/songs /usr/share/nginx/html/songs

EXPOSE 80